# wallpaper-go

A small tool to download the NG Photo of the day and save it 
to `$XDG_DATA_HOME/wallpapers` or `~/.local/share/wallpapers`.

Small simple tool created while learning go.
