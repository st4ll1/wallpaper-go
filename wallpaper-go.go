package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/anaskhan96/soup"
)

type image struct {
	url      string
	date     string
	filename string
}

func main() {

	img := getImageData()
	downloadFile(img, true)
}

func downloadFile(img image, copyToWallpaper bool) {
	filename := getWallpapersDir() + img.filename
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	resp, err := http.Get(img.url)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer resp.Body.Close()

	if _, err := io.Copy(file, resp.Body); err != nil {
		fmt.Println(err)
		os.Exit(3)
	}

	if copyToWallpaper == false {
		os.Exit(0)
	}

	link := getWallpapersDir() + "../wallpaper.jpg"
	os.Remove(link)
	os.Symlink(filename, link)
	fmt.Println("Symlink:")
	fmt.Println(filename, "->" ,link)

}

func getWallpapersDir() string {
	defaultDataPath := os.Getenv("HOME") + "/.local/share"
	dataPath := os.Getenv("XDG_DATA_HOME")
	if dataPath == "" {
		dataPath = defaultDataPath
	}
	wallpaperPath := dataPath + "/wallpapers/"
	os.MkdirAll(wallpaperPath, os.ModeDir)

	return wallpaperPath
}

func getImageData() image {
	resp, err := soup.Get("https://www.nationalgeographic.com/photography/photo-of-the-day")

	if err != nil {
		os.Exit(1)
	}
	doc := soup.HTMLParse(resp)
	targeturl := doc.Find("meta", "property", "og:url").Attrs()["content"]
	imageurl := doc.Find("meta", "property", "og:image").Attrs()["content"]
	imagedate := doc.Find("meta", "property", "gsa_publish_date").Attrs()["content"]

	u, err := url.Parse(targeturl)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	split := strings.Split(u.Path, "/")

	imagetitle := split[len(split)-2]
	filename := imagedate + "-" + imagetitle + ".jpg"

	return image{imageurl, imagedate, filename}
}
